var game = new Phaser.Game(800, 500, Phaser.AUTO, '', { preload: preload, create: create, update: update });


function preload() {
    game.load.image('ball','img/ball.png');
    game.load.image('paddle','img/bramka.png');

}

function create() {
    var style = { font: "15px Arial", fill: "#ff0044", align: "center" };
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.world.enableBody = true;
    this.left = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
    this.right = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);

    this.paddle = game.add.sprite(200, 400, 'paddle');
    this.paddle.body.immovable = true;

    this.ball = game.add.sprite(200, 300, 'ball');
    this.ball.body.velocity.x = 200;
    this.ball.body.velocity.y = 200;
    this.ball.body.bounce.setTo(1);
    this.ball.body.collideWorldBounds = true;

    this.paddle.body.immovable = true;




}

function update() {
    if (this.left.isDown) this.paddle.body.velocity.x = -300;
    else if (this.right.isDown) this.paddle.body.velocity.x = 300;
    else this.paddle.body.velocity.x = 0;
    game.physics.arcade.collide(this.paddle, this.ball);
    if (checkOverlap(this.ball, this.paddle))
    {

    }

}
function hit(ball, paddle) {
    console.log("zderzenie");
    
}
function checkOverlap(spriteA, spriteB) {

    var boundsA = spriteA.getBounds();
    var boundsB = spriteB.getBounds();

    return Phaser.Rectangle.intersects(boundsA, boundsB);

}